package simenka.lab;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 * Class which contains the logic behind Epsilon-NKA simulator.
 * Contains a main class used to insert data used to run simulations.
 * Data is expected as argument to main method.
 * @author Šimun Matešić
 *
 */
public class SimEnka {

	private Set<String> allStates;
	private Set<String> acceptedStates;
	private String firstState;
	private Set<String> allSymbols;
	private List<String> allInputs;
	private StringBuilder sb;
	/**
	 * The current active states.
	 */
	private Set<String> currentStates;
	/**
	 * A Map which has a set of states as a value, and a tuple of (state, input) as key.
	 * It is used to determine which set of states is next for a given input.
	 */
	private Map<Tuple<String,String>,Set<String>> productionRules;
	private static final int INPUTS_INDEX = 0;
	private static final int STATES_INDEX = 1;
	private static final int SYMBOLS_INDEX = 2;
	private static final int ACCEPTED_STATES_INDEX = 3;
	private static final int FIRST_STATE_INDEX = 4;
	private static int counter = 5; // used as an index to array of lines.
	
	/**
	 * Constructor for Enka Simulator class.
	 * Takes input string.
	 * @param inputData
	 */
	public SimEnka(String[] inputData) {
		allStates = new TreeSet<>();
		acceptedStates = new TreeSet<>();
		allSymbols = new TreeSet<>();
		allInputs = new ArrayList<>();
		currentStates = new TreeSet<>();
		productionRules = new HashMap<>();
		extractData(inputData);
	}
	/**
	 * Method which executes the production defined by a state and character.
	 * Takes arguments stateName and inputCharacter. These determine which production will be given.
	 * Returns a set of states.
	 * @param stateName - The state from which this method returns new states.
	 * @param inputCharacter - The character which determines what states are returned.
	 * @return Set<String> - Set of states which are results of a given production.
	 */
	private Set<String> executeProduction(String stateName, String inputCharacter){
		Tuple<String,String> keyPair = new Tuple<>(stateName,inputCharacter);
		if(productionRules.containsKey(keyPair)) {
			return productionRules.get(keyPair);
		}
		else {
			return null;
		}
	}
	
	/**
	 * Sets the currentStates to contain only the first state, which is the desired starting condition
	 * for the ENKA.
	 */
	private void resetENKA() {
		currentStates.clear();
		currentStates.add(firstState); //Ensures the starting state is correct.
	}
	/**
	 * This method runs the actual simulation.
	 * TODO needs to recognize if the entry string is OK.
	 * This can be done by making a simple iteration over the last set, and checking if
	 * if contains the acceptedStates.
	 * A string builder is used to print out progress in standard output.
	 */
	public void simulateEpsilonNKA() {
		System.err.println("Simulating enka...");

		allInputs.forEach(l->{
			sb = new StringBuilder();
			
			sb.append(firstState);
			resetENKA();  //resets to original state.
			
			String[] symbols = l.split(","); //Contains individual symbols of an input string.
			Set<String> newSet = new TreeSet<>(); //This set is used for the switch between sets of states in productions.
			
			for(String sy : symbols) {	
				sb.append("|");
				currentStates.forEach(s->{ //Iterates over currentStates and makes a set of new states.
				//	System.err.println("current for each");
					if(executeProduction(s,sy) != null) {
						newSet.addAll(executeProduction(s, sy));					
						
						while(true) {  //this infinite loop is used to find epsilon-productions.
							Set<String> helperSet = new TreeSet<>();
							helperSet.addAll(newSet);  //helperSet is used to see if there have been changes to newSet.
														
							Set<String> temp = new TreeSet<>(); //an additional helper.
							
							newSet.forEach(n->{ //Looks for epsilon-productions and adds them to the set.
								
								if(executeProduction(n, "$") != null) {
									temp.addAll(executeProduction(n, "$"));
								}
							});
							
							newSet.addAll(temp); //the temp is used to help store new sets in newSet.
							
							if(helperSet.equals(newSet)) {
								temp.clear(); //Clears the temp variable so it can be used again.
								helperSet.clear();
								break;
							}
							else {
								helperSet.addAll(temp); 
								temp.clear(); //Clears the temp variable so it can be used again.
							}
							
						}
					}
					}
				);
				currentStates.clear();
				
				if(newSet.isEmpty()) { //Checking to see if any productions have been made.
					currentStates.add("#"); //This represents that there were no productions that led to new states.
					sb.append("#");
				}
				else { //If there have been productions, currentStates set is updated.				
					currentStates.addAll(newSet);
					newSet.clear(); //clears the newSet since its content is transfered to currentStates.
					
					Iterator<String> iter = currentStates.iterator();		
					while(iter.hasNext()) { //This part of the code gives the desired output.
						String next = iter.next();
						sb.append(next);
						if(iter.hasNext()) sb.append(",");
					}
					
				}
			}
			System.out.println(sb.toString());
			
		});
		
	}

	/**
	 * This method extracts data from given arguments 
	 * and sets the class variables to expected values.
	 * Also, adds the first state to set of current states.
	 */
	private void extractData(String[] data) {
		System.err.println("Extracting data...\n");
	
		String[] lines = data;
		String[] inputs = lines[INPUTS_INDEX].split("\\|"); 
		
		allInputs.addAll(Arrays.asList(inputs));
		
		String[] states = lines[STATES_INDEX].split(","); 
		allStates.addAll(Arrays.asList(states));
		
		String[] symbols = lines[SYMBOLS_INDEX].split(",");
		allSymbols.addAll(Arrays.asList(symbols));
		
		String[] accepted = lines[ACCEPTED_STATES_INDEX].split(","); 
		if(accepted.length != 0) {
			acceptedStates.addAll(Arrays.asList(accepted));
		}
		
		firstState = lines[FIRST_STATE_INDEX]; 
		currentStates.add(firstState); 
		
		while(counter < lines.length) { //Extracts the production rules.
			//This part extracts the leftmost state and character, and then creates a set containing states that are the result of the production.
			String firstLine = lines[counter++]; 
			String[] lAndRSide = firstLine.split("->");
			String leftState = lAndRSide[0].split(",")[0]; 
			String leftChar = lAndRSide[0].split(",")[1];
			
			Set<String> resultStates = new TreeSet<>();

			resultStates.addAll(Arrays.asList(lAndRSide[1].split(",")));
			Tuple<String,String> pair = new Tuple<>(leftState,leftChar);
			
			productionRules.put(pair,resultStates); 
		}
		counter = 5; //The counter is reset.
	}
}
