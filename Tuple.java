package simenka.lab;
/**
 * Helper class used for epsilon-NKA simulation.
 * @author Šimun Matešić
 *
 * @param <S1>
 * @param <S2>
 */
public class Tuple<S1, S2> {
	public final S1 first;
	public final S2 second;
	public Tuple(S1 s1,S2 s2) {
		this.first = s1;
		this.second = s2;
	}
	public S1 getFirst() {
		return first;
	}
	public S2 getSecond() {
		return second;
	}
	@Override
	public int hashCode() {
		return this.first.hashCode() * this.second.hashCode();
	}
	
	@Override
	public boolean equals(Object o) {
		if(!(o instanceof Tuple)) {
			return false;
		}
		Tuple other = (Tuple)o;
		return this.first.equals(other.getFirst())&&(this.second.equals(other.getSecond()));
	}
}
