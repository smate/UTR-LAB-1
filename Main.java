package simenka.lab;
/**
 * This class is used to run the actual simulation in the main method.
 * @author Šimun Matešić
 *
 */
public class Main {
	public static void main(String[] args) {
		SimEnka simulator = new SimEnka(args);
		simulator.simulateEpsilonNKA();
	}
}
